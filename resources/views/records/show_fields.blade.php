<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $record->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $record->name }}</p>
</div>

<!-- Record Field -->
<div class="form-group">
    {!! Form::label('record', 'Record:') !!}
    <p>{{ $record->record }}</p>
    <audio class="audio" preload="none" style="width: 100%; " controls="controls">
                                <source type="audio/mpeg" src="{{asset('audio/'. $record->record)}}"/>
                            </audio>
</div>

