<div class="table-responsive-sm">
    <table class="table table-striped" id="records-table">
        <thead>
            <th>Name</th>
        <th>Record</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($records as $record)
            <tr>
                <td>{{ $record->name }}</td>
            <td>{{ $record->record }}    <audio class="audio" preload="none" style="width: 100%; " controls="controls">
                                <source type="audio/mpeg" src="{{asset('audio/'. $record->record)}}"/>
                            </audio>    </td>
                <td>
                    {!! Form::open(['route' => ['records.destroy', $record->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('records.show', [$record->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('records.edit', [$record->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>