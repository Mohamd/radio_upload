<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Record Field -->
<div class="form-group col-sm-6">
    {!! Form::label('record', 'Record:') !!}
    {!! Form::file('record') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('records.index') }}" class="btn btn-secondary">Cancel</a>
</div>
