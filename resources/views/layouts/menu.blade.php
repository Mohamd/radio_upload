<li class="nav-item {{ Request::is('sliders*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('sliders.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sliders</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Courses</span>
    </a>
</li>
<li class="nav-item {{ Request::is('statics*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('statics.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Statics</span>
    </a>
</li>
<li class="nav-item {{ Request::is('contactuses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('contactuses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Contactus</span>
    </a>
</li>
<li class="nav-item {{ Request::is('records*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('records.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>popular Records</span>
    </a>
</li>

<li class="nav-item {{ Request::is('blogs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('blogs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Blogs</span>
    </a>
</li>


<li class="nav-item {{ Request::is('voices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('voices.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Voices</span>
    </a>
</li>




<br>
<ul class="c-sidebar-nav">Gallery

<li class="nav-item {{ Request::is('images*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('images.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Images</span>
    </a>
</li>
<li class="nav-item {{ Request::is('videos*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('videos.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Videos</span>
    </a>
</li>
</ul>