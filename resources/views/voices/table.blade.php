<div class="table-responsive-sm">
    <table class="table table-striped" id="voices-table">
        <thead>
            <th>Record</th>
        <th>Name</th>
        <th>Text</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($voices as $voice)
            <tr>
            <td>{{ $voice->record }}    <audio class="audio" preload="none" style="width: 100%; " controls="controls">
                                <source type="audio/mpeg" src="{{asset('audio/'. $voice->record)}}"/>
                            </audio>    </td>
            <td>{{ $voice->name }}</td>
            <td>{{ $voice->text }}</td>
                <td>
                    {!! Form::open(['route' => ['voices.destroy', $voice->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('voices.show', [$voice->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('voices.edit', [$voice->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>