<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $voice->id }}</p>
</div>

<!-- Record Field -->
<div class="form-group">
    {!! Form::label('record', 'Record:') !!}
    <p>{{ $voice->record }}</p>
    <audio class="audio" preload="none" style="width: 100%; " controls="controls">
                                <source type="audio/mpeg" src="{{asset('audio/'. $voice->record)}}"/>
                            </audio>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $voice->name }}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{{ $voice->text }}</p>
    
</div>

