<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $videos->id }}</p>
</div>

<!-- Video Field -->
<div class="form-group">
    {!! Form::label('video', 'Video:') !!}
    <video width="320" height="240" controls>
  <source src="{{asset('/videos/'.$videos->video)}}" type="video/mp4">
</video>
</div>

