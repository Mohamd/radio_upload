<div class="table-responsive-sm">
    <table class="table table-striped" id="videos-table">
        <thead>
            <th>Video</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($videos as $videos)
            <tr>
                <td>
                <video width="320" height="240" controls>
  <source src="{{asset('/videos/'.$videos->video)}}" type="video/mp4">
</video>
                </td>
          
                <td>
                    {!! Form::open(['route' => ['videos.destroy', $videos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('videos.show', [$videos->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('videos.edit', [$videos->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>