@extends('blades.master')
<style>
.wrap {
    display: flex;
    background: white;
    padding: 1rem 1rem 1rem 1rem;
    border-radius: 0.5rem;
    box-shadow: 7px 7px 30px -5px rgba(0,0,0,0.1);
    margin-bottom: 2rem;
    margin-top:30px;
}

.wrap:hover {
    background: linear-gradient(135deg,#6394ff 0%,#0a193b 100%);
    color: white;
}

.ico-wrap {
    margin: auto;
}

.mbr-iconfont {
    font-size: 4.5rem !important;
    color: #313131;
    margin: 1rem;
    padding-right: 1rem;
}
.vcenter {
    margin: auto;
}

.mbr-section-title3 {
    text-align: left;
}
h2 {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
    color: black !important;
}
.display-5 {
    font-family: 'Source Sans Pro',sans-serif;
    font-size: 1.4rem;
}
.mbr-bold {
    font-weight: 700;
}

 p {
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    line-height: 25px;
}
.display-6 {
    font-family: 'Source Sans Pro',sans-serif;
    font-size: 1re}

</style>
@section('content')

<div id="content">
        <div class="container">

            <h3 class="animated" data-animation="fadeInUp" data-animation-delay="100">عن الراديو</h3>


            <div class="thumb6 animated" data-animation="fadeInLeft" data-animation-delay="200">
                <div class="thumbnail clearfix">
                    <figure class="">
                        <img src="images/style07.jpg" alt="" class="img-responsive">
                    </figure>
                    <div class="caption">
                        
                        <p>
                          {{$aboutus[0]->value}}
                        </p>
                    </div>
                </div>
            </div>

       
<!-- mission -->
<div class="row mbr-justify-content-center">

<div class="col-lg-6 mbr-col-md-10" style="color: black;">
    <div class="wrap">
        <div class="ico-wrap">
            <span class="mbr-iconfont fa-volume-up fa"></span>
        </div>
        <div class="text-wrap vcenter">
            <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 text-center" style="font-size: 20px;">الهدف </h2>
            <p class="mbr-fonts-style text1 mbr-text display-6">{{$mission[0]->value}}</p>
        </div>
    </div>
</div>
<div class="col-lg-6 mbr-col-md-10" style="color: black;">
    <div class="wrap">
        <div class="ico-wrap">
            <span class="mbr-iconfont fa-calendar fa"></span>
        </div>
        <div class="text-wrap vcenter">
            <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 text-center" style="font-size: 20px;"> الرؤية
                
            </h2>
            <p class="mbr-fonts-style text1 mbr-text display-6">{{$vision[0]->value}}</p>
        </div>
    </div>
</div>


        </div>
    </div>

@stop