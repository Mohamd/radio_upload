@extends('blades.master')
@section('content')

<div id="content">
        <div class="container">

            <h2>{{$blog[0]->title}}</h2>

            <div class="details2 clearfix">
                <!-- <div class="left">نشر في <span>||</span> <?php // if($blog[0]->created_at != $blog[0]->updated_at){echo  date_format($blog[0]["updated_at"],'d M Y'); }else{ echo date_format($blog[0]["created_at"],'d M Y');}?></div> -->
                <!-- <div class="right"><a href="#" class="share1"></a></div> -->
            </div>


            <div class="row">
                <div class="col-sm-3">
                    <!-- <div class="blog_sidebar">

                        <div class="title5">LATEST PHOTOS</div>

                        <ul class="latest2 clearfix">
                            <li><a href="#">
                                <figure><img src="images/ph01.jpg" alt="" class="img-responsive"><em></em></figure>
                            </a></li>
                            <li><a href="#">
                                <figure><img src="images/ph02.jpg" alt="" class="img-responsive"><em></em></figure>
                            </a></li>
                            <li><a href="#">
                                <figure><img src="images/ph03.jpg" alt="" class="img-responsive"><em></em></figure>
                            </a></li>
                            <li><a href="#">
                                <figure><img src="images/ph04.jpg" alt="" class="img-responsive"><em></em></figure>
                            </a></li>
                            <li><a href="#">
                                <figure><img src="images/ph05.jpg" alt="" class="img-responsive"><em></em></figure>
                            </a></li>
                            <li><a href="#">
                                <figure><img src="images/ph06.jpg" alt="" class="img-responsive"><em></em></figure>
                            </a></li>
                        </ul>

                        <div class="title5">LATEST NEWS</div>

                        <div class="news2_wrapper">
                            <div class="news2">
                                <a href="#">
                                    <div class="txt1">Another Blog Post</div>
                                    <div class="txt2">17 / May / 2016</div>
                                </a>
                            </div>
                            <div class="news2">
                                <a href="#">
                                    <div class="txt1">Another Blog Post</div>
                                    <div class="txt2">12 / March / 2016</div>
                                </a>
                            </div>
                            <div class="news2">
                                <a href="#">
                                    <div class="txt1">Another Blog Post</div>
                                    <div class="txt2">13 / Januarry / 2016</div>
                                </a>
                            </div>
                        </div>

                        <div class="title5">CATEGORIES</div>

                        <ul class="cat1">
                            <li><a href="#">Art</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Podcast</a></li>
                            <li><a href="#">Other infomration</a></li>
                        </ul>

                        <div class="title5">TAGS</div>

                        <div class="tags2"><a href="#">Brand</a> <span>\</span> <a href="#">WordPress</a> <span>\</span>
                            <a href="#">Design</a> <span>\</span> <a href="#">Graphic</a> <span>\</span> <a
                                    href="#">PSD</a> <span>\</span> <a href="#">WebSite</a></div>


                    </div> -->
                </div>
<div class="col-sm-9">
                    <div class="blog_content">

                        <div class="post post-short">
                            <div class="post-header">
                                <div class="post-image">
                                    <img src="{{asset('images/'.$blog[0]->image)}}" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="post-story">
                                <div class="post-story-body clearfix">
                                    <p>
                                       {!! $blog[0]->des !!}
                                    </p>

                                   
                               

                                </div>
                                <!-- <div class="post-story-link">
                                    <a href="post.html" class="btn-default btn6">PREVIEW POST</a>
                                </div> -->
                            </div>
                        </div>

                      


                    </div>
                </div>
            </div>


        </div>
    </div>

@stop