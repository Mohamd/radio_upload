@extends('blades.master')
@section('content')

<div id="content">
        <div class="container">

            
            <div class="title2 animated" data-animation="fadeInUp" data-animation-delay="200">فيديوهات</div>
            

            <br><br><br>

            <div class="row">
                @foreach($vid as $c)
                <div class="col-sm-4">
                    <div class="artists1 clearfix animated" data-animation="fadeInUp" data-animation-delay="300">
                       <video width="320" height="240" controls>
  <source src="{{asset('/videos/'.$c->video)}}" type="video/mp4">
</video>
                        
                    </div>
                </div>
                @endforeach
              
              
            </div>

        </div>
    </div>


@stop