
<!DOCTYPE html>
<html lang="en">

<head>
    <title>medista</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your keywords">
    <meta name="author" content="Volcano Agency (m)">

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/camera.css" rel="stylesheet">
    <link href="css/audio4_html5.css" rel="stylesheet">
    <link href="css/mediaelementplayer.css" rel="stylesheet">
    <link href="css/slick.css" rel="stylesheet">
    <link href="css/slick-theme.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Arabic Font-->
    <link href="https://fonts.googleapis.com/css?family=Tajawal&display=swap" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    <style>
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            background-color:#25d366;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            font-size:30px;
            box-shadow: 2px 2px 3px #999;
            z-index:100;
        }

        .my-float{
            margin-top:16px;
        }

        @media (max-width: 991px) {
        .song1_wrapper {
            height: 120px!important;
        }
        }
    </style>



</head>
<body class="onepage front" data-spy="scroll" data-target="#top1" data-offset="92">

<div id="load"></div>

<div id="main">
    <div id="top1">
        <div class="top2_wrapper" id="top2">
            <!-- <a href="#home" class="logo3 scroll-to">

                </a> -->

            <div class="container">
                <header>

                </header>
                <div class="top2 clearfix">
                    <div id="home">


                        <!-- <div class="menu_wrapper">
                            <div class="add1 clearfix">
                                <div class="icon-search"><a href="#"></a></div>
                                <div class="dropdown dropdown1">
                                    <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="true">EN
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="javascript:void(0);">FR</a></li>
                                        <li><a href="javascript:void(0);">DE</a></li>
                                        <li><a href="javascript:void(0);">ES</a></li>
                                    </ul>
                                </div>
                            </div> -->
                        <div class="navbar navbar_ navbar-default">
                            <img src="images/logo3.png" alt="" class="" style="
   width: 20%; float: left;">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="navbar-collapse navbar-collapse_ collapse">
                                <ul class="nav navbar-nav sf-menu clearfix">
                                    <li><a href="/">الرئيسية</a></li>
                                    <li><a href="Aboutus">عن الراديو</a></li>
                                    <li class="sub-menu sub-menu-1"><a href="">المعرض<em></em></a>
                                        <ul>
                                            <li><a href="Gallery-Images">الصور</a></li>
                                            <li><a href="Gallery-Videos">الفيديوهات</a></li>


                                        </ul>
                                    </li>                                    <li><a href="LibraryVoice">المكتبة الصوتية</a></li>
                                    <li><a href="Courses">كورسات</a></li>
                                    <li><a href="Blogs">المدونات</a></li>

                                    <!-- <li class="sub-menu sub-menu-1"><a href="javascript:void(0);">pages<em></em></a>
                                        <ul>
                                            <li><a href="about.html">ABOUT US PAGE</a></li>
                                            <li><a href="artists.html">ARTISTS PAGE</a></li>
                                            <li><a href="mobile-aplication.html">MOBILE APPLICATION</a></li>

                                            <li><a href="blog.html">BLOG PAGE</a></li>
                                            <li><a href="post.html">BLOG POST PAGE</a></li>
                                            <li><a href="channel.html">CHANNEL PAGE</a></li>
                                            <li><a href="styles.html">STYLES PAGE</a></li>
                                        </ul>
                                    </li> -->
                                    <!-- <li><a href="#testimonial">Testimonial</a></li> -->
                                    <li><a href="Contactus">تواصل معنا</a></li>
                                </ul>

                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>

    </div>

























    @yield('content')


    <a href="https://wa.me/20{!!$phone[0]->value!!}" target="_blank" class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>
    <div class="bot1_wrapper">
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-sm-4">

                    <div class="logo2_wrapper">
                        <a href="index-2.html" class="logo2">
                            <img src="images/logo3.png" alt="" class="img-responsive" style="width: 25%;">
                        </a>
                        <!-- <a href="https://api.whatsapp.com/send?phone=01111748937">Send Message</a> -->
                    </div>

                    <p style="color: white;">
                        {!!$aboutus[0]->value!!}
                    </p>

                    <div class="location1">{!!$address[0]->value!!} </div>

                    <div class="phone1">{!!$phone[0]->value!!}</div>

                    <div class="mail1"><a href="#">{!!$email[0]->value!!}</a></div>

                </div>
                <div class="col-sm-3">

                    <div class="bot1_title">الاقسام</div>

                    <ul class="tags1 clearfix">
                        <li><a href="Courses">كورسات</a></li>
                        <li><a href="Aboutus">عن الراديو</a></li>
                        <li><a href="Contactus">تواصل معنا</a></li>

                    </ul>

                    <!-- <div class="bot1_title">MAILING LIST</div>

                    <div class="newsletter_block">
                        <div class="txt1">Lorem ipsum dolor sit amet concateur non troppo sustenuto largo pensare.</div>
                        <div class="newsletter-wrapper clearfix">
                            <form class="newsletter" action="javascript:void(0);">
                                <input type="text" name="s" value='Email Address'
                                       onBlur="if(this.value=='') this.value='Email Address'"
                                       onFocus="if(this.value =='Email Address' ) this.value=''">
                                <a href="javascript:void(0);"></a>
                            </form>
                        </div>
                        <div class="txt2">We respect your privacy</div>
                    </div> -->

                </div>
                <div class="col-sm-4 col-sm-offset-1">

                    <div class="bot1_title">المدونات</div>
                    @foreach($blogs as $b)
                        <div class="latest1">
                            <a href="Blog-details-{{$b->id}}" class="clearfix">
                                <figure><img src="{{asset('/images/' . $b->image)}}" alt="" style="width:30%"></figure>
                                <div class="caption">
                                    <div class="txt1" style="height:30px;">{{ $b->title }}
                                    </div>
                                    <!-- <div class="txt2">January 17 - 2016</div> -->
                                </div>
                            </a>
                        </div>
                    @endforeach





                </div>
            </div>
        </div>
    </div>

    <div class="bot2_wrapper">
        <div class="container">
            <div>  Copyright © 2020 Designed by: <a href="#" target="_blank"><b>Volcano</b></a> </div>

        </div>

    </div>



</div>


<div  >
<footer  style="position: fixed;bottom: 0;left: 0;" id="player">

    <div class="container">
        <ul id="gecmis"></ul>
        <div id="bilgiler">

            <div class="radyo"><strong></strong></div>
            <br>



        </div>
        <div id="kontrol" data-toggle="tooltip" data-placement="top" title="Play / Stop">
            <i class="play glyphicon glyphicon-play"></i>
        </div>
        <div id="sag">
            <div class="yenile" data-toggle="tooltip" data-placement="top" title="Mute/UnMute"><i class="fa fa-volume-up"></i></div>



        </div>
    </div>

</footer>
</div>


<video id="videoElement" style="display: none"></video>





<script src="https://cdn.bootcss.com/flv.js/1.5.0/flv.min.js"></script>



<script src="js/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/superfish.js"></script>

<script src="js/camera.js"></script>

<script src="js/mediaelement-and-player.js"></script>
<script src="js/mep-feature-playlist.js"></script>
<script src="js/slick.min.js"></script>

<script src="js/jquery.jrumble.1.3.min.js"></script>

<script src="js/jquery.sticky.js"></script>

<script src="js/jquery.queryloader2.js"></script>

<script src="js/jquery.appear.js"></script>

<script src="js/jquery.ui.totop.js"></script>
<script src="js/jquery.equalheights.js"></script>

<script src="js/jquery.caroufredsel.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>

{{--<script src="js/SmoothScroll.js"></script>--}}

<script src="js/cform.js"></script>



<script src="js/jquery.mousewheel.min.js" type="text/javascript"></script>


<script src="js/scripts.js"></script>


<script>

    $('#kontrol').click(function ( ) {
        if ($('.play').hasClass('glyphicon-play')) {
            document.getElementById('videoElement').play();
            $('.play').addClass('glyphicon-pause').removeClass('glyphicon-play');
        }  else {
            document.getElementById('videoElement').pause();
            $('.play').addClass('glyphicon-play').removeClass('glyphicon-pause');
        }


    });


    $('.yenile').click(function () {
        var my_element = document.getElementById('videoElement');
        my_element.volume >= 1 ? my_element.volume = 0 : my_element.volume = 1;
    });
    if (flvjs.isSupported()) {
        var videoElement = document.getElementById('videoElement');
        var flvPlayer = flvjs.createPlayer({
            type: 'flv',
            url: 'ws://medistafm.com:8000/live/my_stream.flv'
        });
        flvPlayer.attachMediaElement(videoElement);
        flvPlayer.load();

    }

</script>

</body>


</html>


