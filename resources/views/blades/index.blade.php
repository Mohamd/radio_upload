@extends('blades.master')
<style>
.audio2 .mejs-controls .mejs-play button {
    width: 100%;
    height: 40px;
    border:2px solid #171716 !important;
    #090909;
    background: url(../images/btn-play.png) center center no-repeat;

}
.audio2 .mejs-controls .mejs-play button:hover {

background-color: #0f0f0f !important;
}
</style>
@section('content')


    <div class="add1 add2 clearfix">
        <div class="icon-search"><a href="#"></a></div>
        <div class="dropdown dropdown1">
            <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">EN
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="javascript:void(0);">FR</a></li>
                <li><a href="javascript:void(0);">DE</a></li>
                <li><a href="javascript:void(0);">ES</a></li>
            </ul>
        </div>
    </div>

    <!-- slider -->
    <div id="slider_wrapper">
        <div class="go-down"><a href="#featured" class="scroll-to"></a></div>
        <div class="">
            <div id="slider_inner" class="clearfix">
                <div id="slider" class="clearfix">
                    <div id="camera_wrap">
                        @foreach($slider as $s)
                            <div data-src="{{asset('images/'.$s->image )}}">
                                <div class="camera_caption fadeFromLeft nav1">
                                    <div class="txt1 txt">{{$s->title}}</div>

                                    <div class="txt4">{{$s->text}}</div>

                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>


    <div id="featured">
        <div class="container">

            <!-- <div class="title1 animated" data-animation="fadeInUp" data-animation-delay="100">Listen All Music On Radio
                FM - <b>102.1</b></div> -->

            <div class="kand1 animated" data-animation="fadeInUp" data-animation-delay="200"></div>

            <div class="title2 animated" data-animation="fadeInUp" data-animation-delay="300">كورسات</div>



        </div>


        <div class="slick-slider-wrapper">
            <div class="container">
                <div class="slick-slider slider center">


                    @foreach($course as $c)
                        <div>
                            <div class="slick-slider-inner">
                                <figure><img src="{{asset('images/'.$c->image )}}" alt="" class="img-responsive"></figure>
                                <div class="caption">
                                    <div class="txt2"><span>{{$c->name}}</span></div>
                                    <div class="txt3"><a href="Course-{{$c->id}}" class="btn-default btn1">المزيد</a></div>
                                </div>
                                <div class="slick-slider-overlay"></div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>




        <!-- popular -->


        <div class="container-fluid" style="background-color:#d7df21;" >

            <div class="title2 animated" data-animation="fadeInUp" data-animation-delay="100" style="color:black">الاكثر استماعا
            </div>

            <!-- <div class="title3 animated" data-animation="fadeInUp" data-animation-delay="200">Lorem ipsum dolor sit amet
                concateur non tropp sit namo, allegro sustenuto al prada bravo pensare, chicco milo naturo<br>el spresso
                concateur non value maro noro strata.
            </div> -->

            <br><br><br>

            <div class="radios animated" data-animation="fadeInUp" data-animation-delay="300">




                @foreach($record as $r)
                    <div class="radio1 clearfix">
                        <div class="sec1"></div>
                        <div class="sec2"></div>
                        <div class="sec3"></div>
                        <div class="sec4"style="color:black">{{$r->name}}</div>
                        <div class="sec5"><a href="https://audiojungle.net/item/jazz-samba-5/19377766?s_rank=1"></a></div>
                        <div class="sec6"></div>
                        <div class="sec7">
                            <div class="audio2">
                                <audio class="audio" preload="none" style="width: 100%; visibility: hidden;"
                                       controls="controls">
                                    <source type="audio/mpeg" src="{{asset('audio/'.$r->record)}}"/>
                                    <a href="audio/song2.mp3">audio/19377766_jazz-samba-5_by_abmode_preview.mp3</a>
                                </audio>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="pager_wrapper animated" data-animation="fadeInUp" data-animation-delay="600">
                <!-- <ul class="pager clearfix">
                    <li class="prev"><a href="#">Previous</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li class="li"><a href="#">2</a></li>
                    <li class="li"><a href="#">3</a></li>
                    <li class="li"><a href="#">4</a></li>
                    <li class="next"><a href="#">Next</a></li>
                </ul> -->
                {!!$record->render()!!}
            </div>

        </div>


    </div>


    <!-- <div id="testimonial">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <div class="speaker-wrapper">
                        <img src="images/speaker.jpg" alt="" class="img-responsive">

                        <div class="speaker-img"><img src="images/speaker.jpg" alt="" class="img-responsive"></div>
                    </div>
                </div>
                <div class="col-sm-6">

                    <div id="testim_wrapper">

                        <div class="title1">What You say About us ?</div>
                        <div class="title2">TESTIMONIALS</div>

                        <div id="testim">
                            <div class="carousel-box">
                                <div class="inner">
                                    <div class="carousel main">
                                        <ul>
                                            <li>
                                                <div class="testim">
                                                    <div class="testim_inner">
                                                        <div class="txt1">“Lorem ispum dolor sit amet, concateur u naro
                                                            prado non troppo, amoro sit amet incorporate alido nado
                                                            sempre pensare. Sansunet trifono
                                                            terasimo nanto all trapo, ritenuto largo pensare.” 
                                                        </div>
                                                        <div class="txt2">Amanda Hane Doe</div>
                                                        <div class="txt3">Art Director</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="testim">
                                                    <div class="testim_inner">
                                                        <div class="txt1">“Lorem ispum dolor sit amet, concateur u naro
                                                            prado non troppo, amoro sit amet incorporate alido nado
                                                            sempre pensare. Sansunet trifono
                                                            terasimo nanto all trapo, ritenuto largo pensare.” 
                                                        </div>
                                                        <div class="txt2">Amanda Hane Doe</div>
                                                        <div class="txt3">Art Director</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="testim">
                                                    <div class="testim_inner">
                                                        <div class="txt1">“Lorem ispum dolor sit amet, concateur u naro
                                                            prado non troppo, amoro sit amet incorporate alido nado
                                                            sempre pensare. Sansunet trifono
                                                            terasimo nanto all trapo, ritenuto largo pensare.” 
                                                        </div>
                                                        <div class="txt2">Amanda Hane Doe</div>
                                                        <div class="txt3">Art Director</div>
                                                    </div>
                                                </div>
                                            </li>


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testim_pagination"></div>
                    </div>

                </div>
            </div>

        </div>
    </div> -->

    <div id="collection2">
        <div class="container">

            <!-- <div class="title1 animated" data-animation="fadeInUp" data-animation-delay="100">Listen it from
                www.Yoursite.com
            </div> -->

            <div class="title2 animated" data-animation="fadeInUp" data-animation-delay="200">عن الراديو
            </div>

            <!-- <div class="title3 animated" data-animation="fadeInUp" data-animation-delay="300">Lorem ipsum dolor sit amet
                concateur non tropp sit namo, allegro sustenuto al prada bravo pensare, chicco milo naturo<br>el spresso
                concateur non value maro noro strata. notn troppo.
            </div> -->

            <br><br>

            <div class="row">
                <div class="col-sm-6">
                    <div class="thumb7 animated" data-animation="fadeInUp" data-animation-delay="200">
                        <div class="thumbnail clearfix">
                            <figure class="">
                                <img src="images/style010.jpg" alt="" class="img-responsive">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="thumb7 animated" data-animation="fadeInUp" data-animation-delay="300">
                        <div class="thumbnail clearfix">
                            <div class="caption">
                                <!-- <div class="title">Lorem Ipsum Dolor</div> -->
                                <p style="color: white;">
                                    {{$aboutus[0]->value}}
                                </p>
                                <a href="Aboutus" class="btn-default btn5">المزيد</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



    <div id="contacts">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-push-6 animated" data-animation="fadeInUp" data-animation-delay="200">
                    <img src="images/dancer.png" alt="" class="img-responsive dancer">
                </div>
                <div class="col-sm-6 col-sm-pull-6 animated" data-animation="fadeInLeft" data-animation-delay="200">



                    <div class="title2">تواصل معنا</div>

                    <br>




                    <div class="social_wrapper">
                        <ul class="social clearfix">
                            <!-- <li class="nav1"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="nav2"><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="nav3"><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
                        <!-- <li class="nav4"><a href="https://wa.me/20{!!$phone[0]->value!!}"><i class="fa fa-whatsapp"></i></a></li> -->
                        <!-- <a href="https://wa.me/20{!!$phone[0]->value!!}"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> -->

                        </ul>
                    </div>

                    <div id="note"></div>
                    <div id="fields">
                        <form class="form-horizontal" action="/" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="inputName">الاسم</label>
                                <input type="text" class="form-control" id="inputName" name="name" value="الاسم"
                                       onBlur="if(this.value=='') this.value='Full Name'"
                                       onFocus="if(this.value =='Full Name' ) this.value=''">
                            </div>

                            <div class="form-group">
                                <label for="inputEmail">البريد الاكتروني</label>
                                <input type="text" class="form-control" id="inputEmail" name="email"
                                       onBlur="if(this.value=='') this.value='البريد الاكتروني'"
                                       onFocus="if(this.value =='E-mail address' ) this.value=''">
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="inputMessage">الرسالة</label>
                                        <textarea class="form-control" rows="5" id="inputMessage" name="message"
                                                  onBlur="if(this.value=='') this.value='Message'"
                                                  onFocus="if(this.value =='Message' ) this.value=''">الرسالة</textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn-default btn-cf-submit">ارسال</button>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>

@stop


