@extends('blades.master')
@section('content')

<div id="content">
        <div class="container">

            
            <div class="title2 animated" data-animation="fadeInUp" data-animation-delay="200">كورسات</div>
            

            <br><br><br>

            <div class="row">
                @foreach($course as $c)
                <div class="col-sm-4">
                    <div class="artists1 clearfix animated" data-animation="fadeInUp" data-animation-delay="300">
                       <a href="/Course-{{$c->id}}"> <figure><img src="{{asset('images/'.$c->image)}}" alt="{{$c->name}}" class="img-responsive"></figure></a>
                        <div class="caption">
                          
                            <div class="txt2">{{$c->name}}</div>
                           
                        </div>
                    </div>
                </div>
                @endforeach
              
              
            </div>
            {!! $course->render() !!}

        </div>
    </div>


@stop