@extends('blades.master')
@section('content')
<div class="container">

<div class="title2 animated" data-animation="fadeInUp" data-animation-delay="100">المكتبة الصوتية
</div>

<!-- <div class="title3 animated" data-animation="fadeInUp" data-animation-delay="200">Lorem ipsum dolor sit amet
    concateur non tropp sit namo, allegro sustenuto al prada bravo pensare, chicco milo naturo<br>el spresso
    concateur non value maro noro strata.
</div> -->

<br><br><br>

<div class="radios animated" data-animation="fadeInUp" data-animation-delay="300">




    @foreach($voice as $r)
        <div class="radio1 clearfix">
            <div class="sec1"></div>
            <div class="sec2"></div>
            <div class="sec3">{{$r->name}}</div>
            <div class="sec4">{{$r->text}}</div>
            <div class="sec5"><a href="https://audiojungle.net/item/jazz-samba-5/19377766?s_rank=1"></a></div>
            <div class="sec6"></div>
            <div class="sec7">
                <div class="audio2">
                    <audio class="audio" preload="none" style="width: 100%; visibility: hidden;"
                           controls="controls">
                        <source type="audio/mpeg" src="{{asset('audio/'.$r->record)}}"/>
                        <a href="audio/song2.mp3">audio/19377766_jazz-samba-5_by_abmode_preview.mp3</a>
                    </audio>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="pager_wrapper animated" data-animation="fadeInUp" data-animation-delay="600">
    <!-- <ul class="pager clearfix">
        <li class="prev"><a href="#">Previous</a></li>
        <li class="active"><a href="#">1</a></li>
        <li class="li"><a href="#">2</a></li>
        <li class="li"><a href="#">3</a></li>
        <li class="li"><a href="#">4</a></li>
        <li class="next"><a href="#">Next</a></li>
    </ul> -->
</div>

</div>
@stop