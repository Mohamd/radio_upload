<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVoiceRequest;
use App\Http\Requests\UpdateVoiceRequest;
use App\Repositories\VoiceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class VoiceController extends AppBaseController
{
    /** @var  VoiceRepository */
    private $voiceRepository;

    public function __construct(VoiceRepository $voiceRepo)
    {
        $this->voiceRepository = $voiceRepo;
    }

    /**
     * Display a listing of the Voice.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $voices = $this->voiceRepository->paginate(10);

        return view('voices.index')
            ->with('voices', $voices);
    }

    /**
     * Show the form for creating a new Voice.
     *
     * @return Response
     */
    public function create()
    {
        return view('voices.create');
    }

    /**
     * Store a newly created Voice in storage.
     *
     * @param CreateVoiceRequest $request
     *
     * @return Response
     */
    public function store(CreateVoiceRequest $request)
    {
        $input = $request->all();


        $File=$request-> file('record');
        $destination= public_path('/audio/');
         $newfile = rand(1, 999) . $File -> getClientOriginalName();
        $File ->move ($destination , $newfile);
        
        $input["record"]= $newfile;

        $voice = $this->voiceRepository->create($input);

        Flash::success('Voice saved successfully.');

        return redirect(route('voices.index'));
    }

    /**
     * Display the specified Voice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $voice = $this->voiceRepository->find($id);

        if (empty($voice)) {
            Flash::error('Voice not found');

            return redirect(route('voices.index'));
        }

        return view('voices.show')->with('voice', $voice);
    }

    /**
     * Show the form for editing the specified Voice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $voice = $this->voiceRepository->find($id);

        if (empty($voice)) {
            Flash::error('Voice not found');

            return redirect(route('voices.index'));
        }

        return view('voices.edit')->with('voice', $voice);
    }

    /**
     * Update the specified Voice in storage.
     *
     * @param int $id
     * @param UpdateVoiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVoiceRequest $request)
    {
        $voice = $this->voiceRepository->find($id);

        if (empty($voice)) {
            Flash::error('Voice not found');

            return redirect(route('voices.index'));
        }

        $rec=$request->all();
        if($request->hasFile('record')) {
            $File=$request-> file('record');
            $destination= public_path('/audio/');
            $newfile = rand(1, 999) . $File -> getClientOriginalName();
            $File ->move ($destination , $newfile);
            $rec["record"]= $newfile;
            }

        $voice = $this->voiceRepository->update($rec, $id);

        Flash::success('Voice updated successfully.');

        return redirect(route('voices.index'));
    }

    /**
     * Remove the specified Voice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $voice = $this->voiceRepository->find($id);

        if (empty($voice)) {
            Flash::error('Voice not found');

            return redirect(route('voices.index'));
        }

        $this->voiceRepository->delete($id);

        Flash::success('Voice deleted successfully.');

        return redirect(route('voices.index'));
    }
}
