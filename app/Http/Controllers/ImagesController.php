<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImagesRequest;
use App\Http\Requests\UpdateImagesRequest;
use App\Repositories\ImagesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ImagesController extends AppBaseController
{
    /** @var  ImagesRepository */
    private $imagesRepository;

    public function __construct(ImagesRepository $imagesRepo)
    {
        $this->imagesRepository = $imagesRepo;
    }

    /**
     * Display a listing of the Images.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $images = $this->imagesRepository->paginate(10);

        return view('images.index')
            ->with('images', $images);
    }

    /**
     * Show the form for creating a new Images.
     *
     * @return Response
     */
    public function create()
    {
        return view('images.create');
    }

    /**
     * Store a newly created Images in storage.
     *
     * @param CreateImagesRequest $request
     *
     * @return Response
     */
    public function store(CreateImagesRequest $request)
    {
        $input = $request->all();


        $File=$request-> file('image');
        $destination= public_path('/images/');
         $newfile = rand(1, 999) . $File -> getClientOriginalName();
        $File ->move ($destination , $newfile);
        
        $input["image"]= $newfile;

        $images = $this->imagesRepository->create($input);

        Flash::success('Images saved successfully.');

        return redirect(route('images.index'));
    }

    /**
     * Display the specified Images.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $images = $this->imagesRepository->find($id);

        if (empty($images)) {
            Flash::error('Images not found');

            return redirect(route('images.index'));
        }

        return view('images.show')->with('images', $images);
    }

    /**
     * Show the form for editing the specified Images.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $images = $this->imagesRepository->find($id);

        if (empty($images)) {
            Flash::error('Images not found');

            return redirect(route('images.index'));
        }

        return view('images.edit')->with('images', $images);
    }

    /**
     * Update the specified Images in storage.
     *
     * @param int $id
     * @param UpdateImagesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImagesRequest $request)
    {
        $images = $this->imagesRepository->find($id);

        if (empty($images)) {
            Flash::error('Images not found');

            return redirect(route('images.index'));
        }
        $img=$request->all();
        if($request->hasFile('image')) {
            $File=$request-> file('image');
            $destination= public_path('/images/');
            $newfile = rand(1, 999) . $File -> getClientOriginalName();
            $File ->move ($destination , $newfile);
            $img["image"]= $newfile;
            }
        $images = $this->imagesRepository->update($img, $id);

        Flash::success('Images updated successfully.');

        return redirect(route('images.index'));
    }

    /**
     * Remove the specified Images from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $images = $this->imagesRepository->find($id);

        if (empty($images)) {
            Flash::error('Images not found');

            return redirect(route('images.index'));
        }

        $this->imagesRepository->delete($id);

        Flash::success('Images deleted successfully.');

        return redirect(route('images.index'));
    }
}
