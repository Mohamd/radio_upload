<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Courses;
use App\Models\Record;
use App\Models\Blogs;
use App\Models\Videos;
use App\Models\Images;
use App\Models\Voice;

use DB;

class indexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $slider= Slider::all();
        $course= Courses::all();
       $blogs=Blogs::limit(3)->get();
        $record= DB::table('records')->paginate(6);
        return view('blades.index')->with('slider',$slider)
                                    ->with('course',$course)
                                    ->with('record',$record)
                                    ->with('blogs',$blogs);
    }                                   


    public function course($id){

$course= Courses::where('id',$id)->get();
return view('blades.course-details')->with('course',$course);
    }
    public function blogs(){

        $blogs= Blogs::all();
        return view('blades.Blogs')->with('blogs',$blogs);
            }

            public function vid(){

                $vid= Videos::all();
                return view('blades.videos')->with('vid',$vid);
                    }

                    public function img(){

                        $img= Images::all();
                        return view('blades.images')->with('img',$img);
                            }
        
            public function blog($id){

                $blog= Blogs::where('id',$id)->get();
                return view('blades.Blog-details')->with('blog',$blog);
                    }

public function aboutus(){
    return view('blades.aboutus');
}

public function voice(){
    $voice=Voice::all();
    return view('blades.voice')->with('voice',$voice);
}
public function contact(){
    return view('blades.contactus');
}
public function Allcourses(){
    $course = DB::table('courses')->simplePaginate(3);
   // $course= Courses::all()->simplePaginate(1);
    return view('blades.courses')->with(compact('course'));
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
