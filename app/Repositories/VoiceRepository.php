<?php

namespace App\Repositories;

use App\Models\Voice;
use App\Repositories\BaseRepository;

/**
 * Class VoiceRepository
 * @package App\Repositories
 * @version February 27, 2020, 4:59 pm UTC
*/

class VoiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'record',
        'name',
        'text'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Voice::class;
    }
}
