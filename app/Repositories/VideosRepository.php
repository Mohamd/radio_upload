<?php

namespace App\Repositories;

use App\Models\Videos;
use App\Repositories\BaseRepository;

/**
 * Class VideosRepository
 * @package App\Repositories
 * @version February 27, 2020, 1:38 pm UTC
*/

class VideosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'video'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Videos::class;
    }
}
