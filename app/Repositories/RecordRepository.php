<?php

namespace App\Repositories;

use App\Models\Record;
use App\Repositories\BaseRepository;

/**
 * Class RecordRepository
 * @package App\Repositories
 * @version February 19, 2020, 2:30 pm UTC
*/

class RecordRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'record'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Record::class;
    }
}
