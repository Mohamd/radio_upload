<?php

namespace App\Repositories;

use App\Models\Images;
use App\Repositories\BaseRepository;

/**
 * Class ImagesRepository
 * @package App\Repositories
 * @version February 27, 2020, 1:40 pm UTC
*/

class ImagesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Images::class;
    }
}
