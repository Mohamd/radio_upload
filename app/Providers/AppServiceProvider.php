<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

use App\Models\Statics;
use App\Models\Blogs;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $aboutus= Statics::where("code","=", "Aboutus")->get();
        $address= Statics::where("code","=", "address")->get();
        $email= Statics::where("code","=", "email")->get();
        $phone= Statics::where("code","=", "phone")->get();
        $mission= Statics::where("code","=", "mission")->get();
        $vision= Statics::where("code","=", "vision")->get();
        $blogs=Blogs::limit(3)->get();
//dd($blogs);
        View::share('aboutus',$aboutus);
        View::share('address',$address);
        View::share('phone',$phone);
        View::share('email',$email);
        View::share('blogs',$blogs);
        View::share('mission',$mission);
        View::share('vision',$vision);
    }
}
