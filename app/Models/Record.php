<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Record
 * @package App\Models
 * @version February 19, 2020, 2:30 pm UTC
 *
 * @property string name
 * @property string record
 */
class Record extends Model
{

    public $table = 'records';
    public $timestamps = false;



    public $fillable = [
        'name',
        'record'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'record' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
