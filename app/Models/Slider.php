<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Slider
 * @package App\Models
 * @version February 18, 2020, 1:11 pm UTC
 *
 * @property string title
 * @property string text
 * @property string image
 */
class Slider extends Model
{

    public $table = 'slider';
    public $timestamps = false;



    public $fillable = [
        'title',
        'text',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'text' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
