<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Contactus
 * @package App\Models
 * @version February 18, 2020, 3:08 pm UTC
 *
 * @property string name
 * @property string email
 * @property string message
 */
class Contactus extends Model
{

    public $table = 'contactus';
    



    public $fillable = [
        'name',
        'email',
        'message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'message' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
