<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Images
 * @package App\Models
 * @version February 27, 2020, 1:40 pm UTC
 *
 * @property string image
 */
class Images extends Model
{

    public $table = 'images';
    public $timestamps= false;




    public $fillable = [
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
