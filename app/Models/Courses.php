<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Courses
 * @package App\Models
 * @version February 18, 2020, 1:46 pm UTC
 *
 * @property string name
 * @property string des
 * @property string image
 */
class Courses extends Model
{

    public $table = 'courses';
    
    public $timestamps = false;


    public $fillable = [
        'name',
        'des',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'des' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
