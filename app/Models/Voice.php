<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Voice
 * @package App\Models
 * @version February 27, 2020, 4:59 pm UTC
 *
 * @property integer record
 * @property string name
 * @property string text
 */
class Voice extends Model
{

    public $table = 'voice';
    public $timestamps= false;
    



    public $fillable = [
        'record',
        'name',
        'text'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'record' => 'string',
        'name' => 'string',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
