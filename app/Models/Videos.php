<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Videos
 * @package App\Models
 * @version February 27, 2020, 1:38 pm UTC
 *
 * @property string video
 */
class Videos extends Model
{

    public $table = 'videos';
    
public $timestamps= false;


    public $fillable = [
        'video'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'video' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
