<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('blades.index');
});


Route::get('/','indexController@index');
Route::get('/Course-{course}','indexController@course');
Route::get('/Courses','indexController@Allcourses');
Route::get('/Aboutus','indexController@aboutus');
Route::get('/Blogs','indexController@blogs');

Route::get('Gallery-Videos','indexController@vid');
Route::get('Gallery-Images','indexController@img');
Route::get('LibraryVoice','indexController@voice');

Route::get('/Blog-details-{blog}','indexController@blog');

Route::get('/Contactus','indexController@contact');
Route::post("/Contactus","ContactusController@store");


Route::post("/","ContactusController@store");






Auth::routes(['verify' => true]);





Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::prefix('admin')->group(function () {


        Route::get('/home', 'HomeController@index');

Route::resource('sliders', 'SliderController');

Route::resource('courses', 'CoursesController');

Route::resource('statics', 'StaticsController');

Route::resource('contactuses', 'ContactusController');

Route::resource('records', 'RecordController');

Route::resource('blogs', 'BlogsController');

Route::resource('videos', 'VideosController');

Route::resource('images', 'ImagesController');
Route::resource('voices', 'VoiceController');
});
});


